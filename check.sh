#!/bin/bash

if ! command -v docker &> /dev/null
then
    echo "docker could not be found in \$PATH"
    exit 1
fi

echo "everything looks ok"
