#!/bin/bash

cp service/docker-tor-group.service /etc/systemd/system/docker-tor-group.service || exit 1
cp service/restart-docker-tor-group.sh /usr/bin/restart-docker-tor-group.sh || exit 1
chmod +x /usr/bin/restart-docker-tor-group.sh

systemctl enable --now docker-tor-group
