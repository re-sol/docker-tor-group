#!/bin/bash

# Pull alpine image from Docker hub
echo "==> Pulling library/alpine image"
docker pull alpine

read -p "How many containers should be configured: " containers_count

for (( i=1; i<=$containers_count; i++ ))
do
  port=$((29000+$i))
  echo "==> Configuring container $i (port: $port)"
  docker run -dt --restart=always -p $port:9050 --name tor-group-$i alpine
  docker exec -it tor-group-$i apk add tor
  docker exec -it tor-group-$i cp /etc/tor/torrc.sample /etc/tor/torrc
  docker exec -it tor-group-$i chown -R root:root /var/lib/tor
  docker exec -it tor-group-$i sh -c "echo \"SOCKSPort 0.0.0.0:9050\" | tee -a /etc/tor/torrc"
done
