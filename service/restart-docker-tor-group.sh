#!/bin/bash

containers_count=$(docker ps | grep tor-group- | wc -l)

for (( i=1; i<=$containers_count; i++ ))
do
  port=$((29000+$i))
  container_already_running=$(docker exec -it tor-group-$i ps | grep "tor" | wc -l) # >=1 = already running
  
  # If container with Tor is already running
  if [ "$container_already_running" != "0" ]
  then
    continue
  fi
  
  echo "==> Starting container $i (port: $port)"
  docker start tor-group-$i
  docker exec -dt tor-group-$i tor
done

sleep 7200
