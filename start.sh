#!/bin/bash

read -p "How many containers should be started: " containers_count

for (( i=1; i<=$containers_count; i++ ))
do
  port=$((29000+$i))
  echo "==> Starting container $i (port: $port)"
  docker start tor-group-$i
  docker exec -dt tor-group-$i tor
done
